package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.LocationRepository;
import com.noroff.service.administrator.data_access.MatchRepository;
import com.noroff.service.administrator.data_access.TeamRepository;
import com.noroff.service.administrator.data_access.UserRepository;
import com.noroff.service.administrator.models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/match")
@CrossOrigin("*")
public class MatchController {
    private MatchRepository matchRepository = new MatchRepository();
    private TeamRepository teamRepository = new TeamRepository();
    private LocationRepository locationRepository = new LocationRepository();
    private UserRepository userRepository = new UserRepository();

    /**
     * Get all matches
     * @return List of matches
     */
    @GetMapping
    public ResponseEntity<List<Match>> getAllMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Match> matches = matchRepository.getAllMatches();

        if(matches == null || matches.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(matches, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    /**
     * Get specific match
     * @param id The id of the match
     * @return The match
     */
    @GetMapping("/{id}")
    public ResponseEntity<Match> getSpecificMatch(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Match match = matchRepository.getSpecificMatch(id);

        if(match == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(match, status);
    }

    /**
     * Post a new mastch
     * @param match The match to post
     * @return The created match
     */
    @PostMapping()
    public ResponseEntity<Match> postMatch(@RequestBody Match match){
        Pair<HttpStatus, Integer> res = matchRepository.addMatch(match);

        if(res.getU() != -1){
            match.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(match, res.getT());
    }

    /**
     * Put a match
     * @param id The id of the match
     * @param match The new match
     * @return The updated match
     */
    @PutMapping("/{id}")
    public ResponseEntity<Match> putMatch(@PathVariable int id, @RequestBody Match match){
        HttpStatus status = matchRepository.putMatch(id, match);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(match, status);
    }

    /**
     * Delete a match
     * @param id The match to delete
     * @return The deleted match
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Match> deleteMatch(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }


    /**
     * Cancel a match
     * @param id The id of the match
     * @param cancleMatchDTO The message and who cancelled the match
     * @return The cancelled match
     */
    @PatchMapping("/{id}/cancel")
    public ResponseEntity<CancleMatch> cancleMatch(@PathVariable int id, @RequestBody CancleMatchDTO cancleMatchDTO){
        HttpStatus status = matchRepository.cancleMatch(id, cancleMatchDTO.getMessage());

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        Match match = getSpecificMatch(id).getBody();
        String team1Name = teamRepository.getSpecificTeam(match.getTeam_1_id()).getName();
        String team2Name = teamRepository.getSpecificTeam(match.getTeam_2_id()).getName();
        String locationName = locationRepository.getSpecificLocation(match.getLocation_id()).getName();
        CancleMatch returnObj = new CancleMatch(
                match.getStart_time(),
                cancleMatchDTO.getMessage(),
                locationName,
                team1Name,
                team2Name);

        MessageDTO message = new MessageDTO();
        message.setMessage("The event at " + locationName + " at "
                + match.getStart_time()
                + " is canceled. Message: " + cancleMatchDTO.getMessage());
        message.setSender(cancleMatchDTO.getSender());

        messageParticipants(match.getId(), message);

        return new ResponseEntity<>(returnObj, status);
    }

    /**
     * Send a message to participants in a match
     * @param id The match id
     * @param message The message to send with the sender username
     */
    public void messageParticipants(int id, MessageDTO message){
        Match match = getSpecificMatch(id).getBody();

        ArrayList<UserDTO> participants = new ArrayList<>();
        participants.addAll(teamRepository.getAllPlayers(match.getTeam_1_id()));
        participants.addAll(teamRepository.getAllPlayers(match.getTeam_2_id()));

        participants.forEach(user -> {
            userRepository.sendMessage(message.getSender(), user.getUsername(), message.getMessage());
        });
    }
}
