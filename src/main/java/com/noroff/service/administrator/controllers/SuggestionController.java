package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.SuggestionRepository;
import com.noroff.service.administrator.models.Suggestion;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/suggestion")
@CrossOrigin("*")
public class SuggestionController {

    SuggestionRepository suggestionRepository = new SuggestionRepository();

    @GetMapping()
    public ResponseEntity<List<Suggestion>> getAllSuggestions(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Suggestion> suggestions = suggestionRepository.getAllSuggestions();

        if(suggestions == null || suggestions.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(suggestions, status);
        }

        return new ResponseEntity<>(suggestions, status);
    }

    @GetMapping("/pending")
    public ResponseEntity<List<Suggestion>> getAllPendingSuggestions(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Suggestion> suggestions = suggestionRepository.getAllPendingSuggestions();

        if(suggestions == null || suggestions.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(suggestions, status);
        }

        return new ResponseEntity<>(suggestions, status);
    }

    @GetMapping("/approved")
    public ResponseEntity<List<Suggestion>> getAllApprovedSuggestions(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Suggestion> suggestions = suggestionRepository.getAllApprovedSuggestions();

        if(suggestions == null || suggestions.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(suggestions, status);
        }

        return new ResponseEntity<>(suggestions, status);
    }
}
