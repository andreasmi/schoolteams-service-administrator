package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.TeamRepository;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Team;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/team")
@CrossOrigin("*")
public class TeamController {
    private TeamRepository teamRepository = new TeamRepository();

    /**
     * Get all teams
     * @return List of teams
     */
    @GetMapping
    public ResponseEntity<List<Team>> getAllTeams(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> teams = teamRepository.getAllTeams();

        if(teams.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(teams, status);
        }

        return new ResponseEntity<>(teams, status);
    }

    /**
     * Get specific team
     * @param id The team id
     * @return The team
     */
    @GetMapping("/{id}")
    public ResponseEntity<Team> getSpecificTeam(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Team team = teamRepository.getSpecificTeam(id);

        if(team == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(team, status);
    }

    /**
     * Post a new team
     * @param team The team to post
     * @return The created team
     */
    @PostMapping()
    public ResponseEntity<Team> postTeam(@RequestBody Team team){
        Pair<HttpStatus, Integer> res = teamRepository.addTeam(team);

        if(res.getU() != -1){
            team.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(team, res.getT());
    }

    /**
     * Put a team
     * @param id The team id
     * @param team The new team
     * @return The updated team
     */
    @PutMapping("/{id}")
    public ResponseEntity<Team> putTeam(@PathVariable int id, @RequestBody Team team){
        HttpStatus status = teamRepository.putTeam(id, team);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(team, status);
    }

    /**
     * Delete a team
     * @param id The id of the team
     * @return The deleted team
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Team> deleteTeam(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation

        return new ResponseEntity<>(null, status);
    }

    @GetMapping("/school/{school_id}")
    public ResponseEntity<ArrayList<Team>> getTeamBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamBySchool(school_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/sport/{sport_id}")
    public ResponseEntity<ArrayList<Team>> getTeamBySport(@PathVariable int sport_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamBySport(sport_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/coach/{coach}")
    public ResponseEntity<ArrayList<Team>> getTeamByCoach(@PathVariable String coach){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamByCoach(coach);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
