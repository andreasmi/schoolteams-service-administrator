package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.LocationRepository;
import com.noroff.service.administrator.models.Location;
import com.noroff.service.administrator.models.LocationFull;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Postal;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/location")
@CrossOrigin("*")
public class LocationController {

    private LocationRepository locationRepository = new LocationRepository();

    /**
     * Gets all the locations
     * @return List of Location
     */
    @GetMapping
    public ResponseEntity<List<Location>> getAllLocations(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Location> locations = locationRepository.getAllLocations();

        if(locations.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(locations, status);
        }

        return new ResponseEntity<>(locations, status);
    }

    /**
     * Get specific location
     * @param id The location id
     * @return The location
     */
    @GetMapping("/{id}")
    public ResponseEntity<Location> getSpecificLocation(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Location location = locationRepository.getSpecificLocation(id);

        if(location == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(location, status);
    }


    /**
     * Post a new location
     * @param location The location to post
     * @return The new location
     */
    @PostMapping()
    public ResponseEntity<Location> postLocation(@RequestBody Location location){
        HttpStatus status;
        Pair<HttpStatus, Integer> res = locationRepository.addLocation(location);

        if(res.getU() == -1){
            location.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(location, res.getT());
    }

    /**
     * Updates a existing location
     * @param id The id of the location to update
     * @param location The new location
     * @return The updated location
     */
    @PutMapping("/{id}")
    public ResponseEntity<Location> putLocation(@PathVariable int id, @RequestBody Location location){
        HttpStatus status = locationRepository.putLocation(id, location);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(location, status);
    }

    /**
     * Delete a location
     * @param id The location to delete
     * @return The deleted location
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }

    /**
     * Gets all the postals
     * @return List of postal
     */
    @GetMapping("/postal")
    public ResponseEntity<List<Postal>> getAllPostals(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Postal> postals = locationRepository.getAllPostals();

        if(postals.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(postals, status);
        }

        return new ResponseEntity<>(postals, status);
    }

    /**
     * Get specific postal
     * @param postalCode The postal code
     * @return The postal
     */
    @GetMapping("/postal/{postalCode}")
    public ResponseEntity<Postal> getSpecificPostal(@PathVariable int postalCode){
        HttpStatus status = HttpStatus.OK;
        Postal postal = locationRepository.getSpecificPostal(postalCode);

        if(postal == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(postal, status);
    }

    /**
     * Get specific full location
     * @return List of full locations
     */
    @GetMapping("/full")
    public ResponseEntity<ArrayList<LocationFull>> getFullLocation(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<LocationFull> locations = locationRepository.getAllLocationFull();

        if(locations.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(locations, status);
        }

        return new ResponseEntity<>(locations, status);
    }

    /**
     * Get specific postal
     * @return The postal
     */
    @GetMapping("/full/{id}")
    public ResponseEntity<LocationFull> getFullSpecificLocation(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        LocationFull location = locationRepository.getFullSpecificLocation(id);

        if(location == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(location, status);
    }

}
