package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.OptionalRepository;
import com.noroff.service.administrator.models.Optional;
import com.noroff.service.administrator.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/optional")
@CrossOrigin("*")
public class OptionalController {

    private OptionalRepository optionalRepository = new OptionalRepository();

    /**
     * Gets all the optionals
     * @return List of Optional
     */
    @GetMapping
    public ResponseEntity<List<Optional>> getAllOptionals(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Optional> optionals = optionalRepository.getAllOptionals();

        if(optionals.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(optionals, status);
        }

        return new ResponseEntity<>(optionals, status);
    }

    /**
     * Get specific optional
     * @param id The optional id
     * @return The optional
     */
    @GetMapping("/{id}")
    public ResponseEntity<Optional> getSpecificOptional(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getSpecificOptional(id);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }


    /**
     * Post a new optional
     * @param optional The optional to post
     * @return The created optional
     */
    @PostMapping()
    public ResponseEntity<Optional> postOptional(@RequestBody Optional optional){
        Pair<HttpStatus, Integer> res = optionalRepository.addOptional(optional);

        if(res.getU() != -1){
            optional.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(optional, res.getT());
    }

    /**
     * Updates a existing optional
     * @param id The id of the optional to update
     * @param optional The new optional
     * @return The updated optional
     */
    @PutMapping("/{id}")
    public ResponseEntity<Optional> putOptional(@PathVariable int id, @RequestBody Optional optional){
        HttpStatus status = optionalRepository.putOptional(id, optional);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

    /**
     * Delete a optional
     * @param id The optional id
     * @return The deleted optional
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Optional> deleteOptional(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
