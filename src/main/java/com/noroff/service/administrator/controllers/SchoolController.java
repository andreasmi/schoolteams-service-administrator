package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.SchoolRepository;
import com.noroff.service.administrator.models.Location;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.School;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/school")
@CrossOrigin("*")
public class SchoolController {
    private SchoolRepository schoolRepository = new SchoolRepository();

    /**
     * Get all schools
     * @return List of schools
     */
    @GetMapping
    public ResponseEntity<List<School>> getAllSchools(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<School> schools = schoolRepository.getAllSchools();

        if(schools.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(schools, status);
        }

        return new ResponseEntity<>(schools, status);
    }

    /**
     * Get specific school
     * @param id The school id
     * @return The school
     */
    @GetMapping("/{id}")
    public ResponseEntity<School> getSpecificSchool(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        School school = schoolRepository.getSpecificSchool(id);

        if(school == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(school, status);
    }

    /**
     * Post a new school
     * @param school The school to post
     * @return The created school
     */
    @PostMapping()
    public ResponseEntity<School> postSchool(@RequestBody School school){
        Pair<HttpStatus, Integer> res = schoolRepository.addSchool(school);

        if(res.getU() != -1){
            school.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(school, res.getT());
    }

    /**
     * Put a school
     * @param id The school id
     * @param school The new school
     * @return The updated school
     */
    @PutMapping("/{id}")
    public ResponseEntity<School> putSchool(@PathVariable int id, @RequestBody School school){
        HttpStatus status = schoolRepository.putSchool(id, school);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(school, status);
    }

    /**
     * Delete a school
     * @param id The id of the school
     * @return The deleted school
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<School> deleteSchool(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation

        return new ResponseEntity<>(null, status);
    }

}