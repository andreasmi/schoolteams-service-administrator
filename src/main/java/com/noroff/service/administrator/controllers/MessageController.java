package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.MessageRepository;
import com.noroff.service.administrator.models.Match;
import com.noroff.service.administrator.models.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/message")
@CrossOrigin("*")
public class MessageController {
    MessageRepository messageRepository = new MessageRepository();

    @GetMapping("/all")
    public ResponseEntity<List<Message>> getAllMessages(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessages();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    // Messages to/from administrator
    @GetMapping("/sent/administrator")
    public ResponseEntity<List<Message>> getAllMessagesFromAdministrator(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesFromAdministrator();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/sent/administrator/{username}")
    public ResponseEntity<List<Message>> getAllMessagesFromSpecificAdministrator(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesFromSpecificAdministrator(username);

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/administrator")
    public ResponseEntity<List<Message>> getAllMessagesToAdministrator(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesToAdministrator();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/administrator/{username}")
    public ResponseEntity<List<Message>> getAllMessagesToSpecificAdministrator(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesToSpecificAdministrator(username);

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/coach")
    public ResponseEntity<List<Message>> getAllMessagesFromCoachToAdministrator(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesFromCoachToAdministrator();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/parent")
    public ResponseEntity<List<Message>> getAllMessagesFromParentToAdministrator(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesFromParentToAdministrator();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/received/player")
    public ResponseEntity<List<Message>> getAllMessagesFromPlayerToAdministrator(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getAllMessagesFromPlayerToAdministrator();

        if(messages == null || messages.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(messages, status);
        }

        return new ResponseEntity<>(messages, status);
    }
}
