package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.PersonRepository;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/person")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {

    private PersonRepository personRepository = new PersonRepository();

    /**
     * Gets all the persons
     * @return List of person
     */
    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Person> persons = personRepository.getAllPersons();

        if(persons.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(persons, status);
        }

        return new ResponseEntity<>(persons, status);
    }

    /**
     * Get specific person
     * @param id The person id
     * @return The person
     */
    @GetMapping("/{id}")
    public ResponseEntity<Person> getSpecificPerson(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Person person = personRepository.getSpecificPerson(id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }


    /**
     * Post a new person
     * @param person The person to post
     * @return The created person
     */
    @PostMapping()
    public ResponseEntity<Person> postPerson(@RequestBody Person person){
        Pair<HttpStatus, Integer> res = personRepository.addPerson(person);

        if(res.getU() != -1){
            person.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(person, res.getT());
    }

    /**
     * Updates a existing person
     * @param id The id of the person to update
     * @param person The new person
     * @return The updated person
     */
    @PutMapping("/{id}")
    public ResponseEntity<Person> putPerson(@PathVariable int id, @RequestBody Person person){
        HttpStatus status = personRepository.putPerson(id, person);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    /**
     * Delete a person
     * @param id The id of the person to delete
     * @return The deleted person
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Person> deletePerson(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation of delete mapping

        return new ResponseEntity<>(null, status);
    }
}
