package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.OptionalRepository;
import com.noroff.service.administrator.data_access.PersonRepository;
import com.noroff.service.administrator.data_access.SchoolRepository;
import com.noroff.service.administrator.data_access.UserRepository;
import com.noroff.service.administrator.models.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/user")
@CrossOrigin("*")
public class UserController {
    private UserRepository userRepository = new UserRepository();
    private PersonRepository personRepository = new PersonRepository();
    private OptionalRepository optionalRepository = new OptionalRepository();
    private SchoolRepository schoolRepository = new SchoolRepository();

    /**
     * Get all users
     * @return List of user
     */
    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllUsers();

        if(users.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(users, status);
        }

        return new ResponseEntity<>(users, status);
    }

    /**
     * Get a specific user
     * @param username The users username
     * @return The user
     */
    @GetMapping("/{username}")
    public ResponseEntity<User> getSpecificUser(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        User user = userRepository.getSpecificUser(username);

        if(user == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    /**
     * Post a new user
     * @param user The user to post
     * @return The posted user
     */
    @PostMapping()
    public ResponseEntity<User> postUser(@RequestBody User user){
        HttpStatus status = userRepository.addUser(user);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    /**
     * Put a user
     * @param username The users username
     * @param user The user
     * @return The updated user
     */
    @PutMapping("/{username}")
    public ResponseEntity<User> putUser(@PathVariable String username, @RequestBody User user){
        HttpStatus status = userRepository.putUser(username, user);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    /**
     * Delete a user
     * @param username The username
     * @return The deleted user
     */
    @DeleteMapping("/{username}")
    public ResponseEntity<User> deleteUser(@PathVariable String username){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation

        return new ResponseEntity<>(null, status);
    }


    /**
     * Get the all information for a specific user
     * @param username The users username
     * @return A FullUser object where user, person and optional is combined
     */
    @GetMapping("/{username}/full")
    public ResponseEntity<FullUser> getFullUser(@PathVariable String username){
        User user = getSpecificUser(username).getBody();
        Person person = personRepository.getSpecificPerson(user.getPerson_id());
        Optional optional = optionalRepository.getSpecificOptional(person.getOptional_id());

        if(user == null || person == null || optional == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        HttpStatus status = HttpStatus.OK;

        return new ResponseEntity<>(new FullUser(user, person, optional), status);
    }

    /**
     * Send message to every parent in a school
     * @param id The school id
     * @param message MessageDTO with sender and message
     * @return
     */
    @PostMapping("/notify/school/{id}")
    public ResponseEntity<Void> notifyParentsFromSchool(@PathVariable int id, @RequestBody MessageDTO message){
        ArrayList<TeamDTO> teams = schoolRepository.getAllTeamsInSchool(id);

        if(teams.size() == 0){
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }

        teams.forEach(team -> {
            userRepository.notifyParents(team.getId(), message);
        });

        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * Send message to all parents in a team
     * @param id The team id
     * @param message MessageDTO with sender and message
     * @return
     */
    @PostMapping("/notify/team/{id}")
    public ResponseEntity<Void> notifyParentsFromTeam(@PathVariable int id, @RequestBody MessageDTO message){
        HttpStatus status = userRepository.notifyParents(id, message);

        return new ResponseEntity<>(null, status);
    }

    @PostMapping("/child_parent")
    public ResponseEntity<ChildParent> addNewChildParentRelationship(@RequestBody ChildParent childParent){
        HttpStatus status = userRepository.addChildParent(childParent);

        if(status != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(childParent, status);
    }

    @GetMapping("/child_parent/parent/{child_username}")
    public ResponseEntity<ChildParent> addNewChildParentRelationship(@PathVariable String child_username){
        HttpStatus status = HttpStatus.OK;
        ChildParent childParent = userRepository.getChildParent(child_username);

        if(childParent == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(childParent, status);
    }

    /**
     * Toggle two factor active flag
     * @param toggle2FA The toggle object with username and active flag
     * @return The state of the flag
     */
    @PatchMapping("/2fa")
    public ResponseEntity<Boolean> toggleActive2fa(@RequestBody Toggle2FA toggle2FA){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.update2FA(toggle2FA);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        check2faData(toggle2FA.getUsername());

        return new ResponseEntity<>(active, status);
    }

    /**
     * Gets the qr code url for the user
     * @param username The users username
     * @return The url as a string
     */
    @GetMapping("/2fa/qr/{username}")
    public ResponseEntity<TwoFaQr> get2faQrUrl(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        String url = userRepository.getQrUrl(username);

        if(url == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new TwoFaQr(url), status);
    }

    /**
     * Gets the qr code url for the user
     * @param username The users username
     * @return The url as a string
     */
    @GetMapping("/2fa/active/{username}")
    public ResponseEntity<twoFaActive> get2faActive(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.getActive(username);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }


        return new ResponseEntity<>(new twoFaActive(active), status);
    }



    /**
     * Get all users
     * @return List of user
     */
    @GetMapping("/coach")
    public ResponseEntity<List<FullUser>> getAllCoaches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<FullUser> users = userRepository.getAllCoaches();

        if(users.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(users, status);
        }

        return new ResponseEntity<>(users, status);
    }

    /**
     * Get all users
     * @return List of user
     */
    @GetMapping("/administrator")
    public ResponseEntity<List<FullUser>> getAllAdministratorss(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<FullUser> users = userRepository.getAllAdministrators();

        if(users.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(users, status);
        }

        return new ResponseEntity<>(users, status);
    }

    /**
     * Get all users
     * @return List of user
     */
    @GetMapping("/parent")
    public ResponseEntity<List<FullUser>> getAllParents(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<FullUser> users = userRepository.getAllParents();

        if(users.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(users, status);
        }

        return new ResponseEntity<>(users, status);
    }

    private void check2faData(String username){
        TwoFaQr url = get2faQrUrl(username).getBody();
        if(url == null){
            //Sends request to verify the token
            String generateUrl = "https://schoolteams-service-auth.herokuapp.com/api/v1/2fa/generate";
            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> requestBody = new HashMap();
            requestBody.put("username", username);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<?> responseEntity = restTemplate.postForEntity(
                    generateUrl,
                    requestEntity,
                    String.class);
            HttpStatus statusCode = responseEntity.getStatusCode();
            //If the response code is 200, then the token is valid
            boolean authorized = statusCode == HttpStatus.OK;
        }
    }
}
