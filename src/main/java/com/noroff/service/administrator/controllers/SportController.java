package com.noroff.service.administrator.controllers;

import com.noroff.service.administrator.data_access.SportRepository;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Sport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/sport")
@CrossOrigin("*")
public class SportController {
    private SportRepository sportRepository = new SportRepository();

    /**
     * Get all sports
     * @return List of the sports
     */
    @GetMapping
    public ResponseEntity<List<Sport>> getAllSports(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Sport> sports = sportRepository.getAllSports();

        if(sports.size() == 0){
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(sports, status);
        }

        return new ResponseEntity<>(sports, status);
    }

    /**
     * Get specific sport
     * @param id The sport id
     * @return The sport
     */
    @GetMapping("/{id}")
    public ResponseEntity<Sport> getSpecificSport(@PathVariable int id){
        HttpStatus status = HttpStatus.OK;
        Sport sport = sportRepository.getSpecificSport(id);

        if(sport == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(sport, status);
    }

    /**
     * Post a new sport
     * @param sport The new sport
     * @return The created sport
     */
    @PostMapping()
    public ResponseEntity<Sport> postSport(@RequestBody Sport sport){
        Pair<HttpStatus, Integer> res = sportRepository.addSport(sport);

        if(res.getU() != -1){
            sport.setId(res.getU());
        }

        if(res.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, res.getT());
        }

        return new ResponseEntity<>(sport, res.getT());
    }

    /**
     * Put a sport
     * @param id The sport id
     * @param sport The new sport
     * @return The updated sport
     */
    @PutMapping("/{id}")
    public ResponseEntity<Sport> putSport(@PathVariable int id, @RequestBody Sport sport){
        HttpStatus status = sportRepository.putSport(id, sport);

        if(status != HttpStatus.ACCEPTED){
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(sport, status);
    }

    /**
     * Delete a sport
     * @param id The sport id
     * @return The deleted sport
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Sport> deleteSport(@PathVariable int id){
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;

        //TODO: Add implementation

        return new ResponseEntity<>(null, status);
    }
}
