package com.noroff.service.administrator.models;

import java.time.LocalDateTime;

public class Optional {
    private int id;
    private LocalDateTime date_of_birth;
    private String mobile_number;
    private String profile_picture;
    private String medical_notes;
    private Boolean dob_shared;
    private Boolean mobile_number_shared;
    private Boolean profile_picture_shared;
    private Boolean medical_notes_shared;
    //Constructors


    public Optional(int id, LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes, Boolean dob_shared, Boolean mobile_number_shared, Boolean profile_picture_shared, Boolean medical_notes_shared) {
        this.id = id;
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
        this.dob_shared = dob_shared;
        this.mobile_number_shared = mobile_number_shared;
        this.profile_picture_shared = profile_picture_shared;
        this.medical_notes_shared = medical_notes_shared;
    }

    public Optional(LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes, Boolean dob_shared, Boolean mobile_number_shared, Boolean profile_picture_shared, Boolean medical_notes_shared) {
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
        this.dob_shared = dob_shared;
        this.mobile_number_shared = mobile_number_shared;
        this.profile_picture_shared = profile_picture_shared;
        this.medical_notes_shared = medical_notes_shared;
    }

    public Optional() {
    }

    //Getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDateTime date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getMedical_notes() {
        return medical_notes;
    }

    public void setMedical_notes(String medical_notes) {
        this.medical_notes = medical_notes;
    }

    public Boolean getDob_shared() {
        return dob_shared;
    }

    public void setDob_shared(Boolean dob_shared) {
        this.dob_shared = dob_shared;
    }

    public Boolean getMobile_number_shared() {
        return mobile_number_shared;
    }

    public void setMobile_number_shared(Boolean mobile_number_shared) {
        this.mobile_number_shared = mobile_number_shared;
    }

    public Boolean getProfile_picture_shared() {
        return profile_picture_shared;
    }

    public void setProfile_picture_shared(Boolean profile_picture_shared) {
        this.profile_picture_shared = profile_picture_shared;
    }

    public Boolean getMedical_notes_shared() {
        return medical_notes_shared;
    }

    public void setMedical_notes_shared(Boolean medical_notes_shared) {
        this.medical_notes_shared = medical_notes_shared;
    }
}
