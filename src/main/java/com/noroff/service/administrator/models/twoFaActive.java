package com.noroff.service.administrator.models;

public class twoFaActive {
    Boolean active;

    public twoFaActive(Boolean active) {
        this.active = active;
    }

    public twoFaActive() {
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
