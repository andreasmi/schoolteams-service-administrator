package com.noroff.service.administrator.models;

public class Message {
    private int message_id;
    private String receiver;
    private String sender;
    private String message;

    public Message(int message_id, String receiver, String sender, String message) {
        this.message_id = message_id;
        this.receiver = receiver;
        this.sender = sender;
        this.message = message;
    }

    public Message() {
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
