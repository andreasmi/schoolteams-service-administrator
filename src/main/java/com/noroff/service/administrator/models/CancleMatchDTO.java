package com.noroff.service.administrator.models;

public class CancleMatchDTO {
    private String message;
    private String sender;

    public CancleMatchDTO(String message, String sender) {
        this.message = message;
        this.sender = sender;
    }

    public CancleMatchDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
