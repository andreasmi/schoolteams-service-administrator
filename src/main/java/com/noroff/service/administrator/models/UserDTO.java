package com.noroff.service.administrator.models;

public class UserDTO {
    private String username;
    private int person_id;
    private int role_id;

    public UserDTO(String username, int person_id, int role_id) {
        this.username = username;
        this.person_id = person_id;
        this.role_id = role_id;
    }

    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}
