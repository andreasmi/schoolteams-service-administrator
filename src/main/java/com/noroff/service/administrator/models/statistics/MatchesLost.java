package com.noroff.service.administrator.models.statistics;

public class MatchesLost {
    private int id;
    private int matches_lost;

    public MatchesLost(int id, int matches_lost) {
        this.id = id;
        this.matches_lost = matches_lost;
    }

    public MatchesLost() {
    }

    public MatchesLost(int losses) {
        this.matches_lost = losses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches_lost() {
        return matches_lost;
    }

    public void setMatches_lost(int matches_lost) {
        this.matches_lost = matches_lost;
    }
}
