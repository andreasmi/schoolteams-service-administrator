package com.noroff.service.administrator.models;

public class Toggle2FA {
    private String username;
    private Boolean active;

    public Toggle2FA(String username, Boolean active) {
        this.username = username;
        this.active = active;
    }

    public Toggle2FA() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
