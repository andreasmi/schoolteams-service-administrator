package com.noroff.service.administrator.models;

public class ChildParent {
    String childUsername;
    String parent1Username;
    String parent2Username;

    public ChildParent(String childUsername, String parent1Username, String parent2Username) {
        this.childUsername = childUsername;
        this.parent1Username = parent1Username;
        this.parent2Username = parent2Username;
    }

    public ChildParent(String childUsername, String parent1Username) {
        this.childUsername = childUsername;
        this.parent1Username = parent1Username;
        this.parent2Username = null;
    }

    public ChildParent() {
    }

    public String getChildUsername() {
        return childUsername;
    }

    public void setChildUsername(String childUsername) {
        this.childUsername = childUsername;
    }

    public String getParent1Username() {
        return parent1Username;
    }

    public void setParent1Username(String parent1Username) {
        this.parent1Username = parent1Username;
    }

    public String getParent2Username() {
        return parent2Username;
    }

    public void setParent2Username(String parent2Username) {
        this.parent2Username = parent2Username;
    }
}
