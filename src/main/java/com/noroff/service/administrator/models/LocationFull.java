package com.noroff.service.administrator.models;

public class LocationFull {
    private int id;
    private String street;
    private String number;
    private String name;
    private int postalCode_post_code;
    private String postal_city;

    public LocationFull(int id, String street, String number, String name, int postalCode_post_code, String postal_city) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.name = name;
        this.postalCode_post_code = postalCode_post_code;
        this.postal_city = postal_city;
    }

    public LocationFull() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostalCode_post_code() {
        return postalCode_post_code;
    }

    public void setPostalCode_post_code(int postalCode_post_code) {
        this.postalCode_post_code = postalCode_post_code;
    }

    public String getPostal_city() {
        return postal_city;
    }

    public void setPostal_city(String postal_city) {
        this.postal_city = postal_city;
    }
}
