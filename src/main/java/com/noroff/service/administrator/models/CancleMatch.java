package com.noroff.service.administrator.models;

import java.time.LocalDateTime;

public class CancleMatch {
    private LocalDateTime start_time;
    private String comment;
    private String location_name;
    private String team_1_name;
    private String team_2_name;

    public CancleMatch(LocalDateTime start_time, String comment, String location_name, String team_1_name, String team_2_name) {
        this.start_time = start_time;
        this.comment = comment;
        this.location_name = location_name;
        this.team_1_name = team_1_name;
        this.team_2_name = team_2_name;
    }

    public CancleMatch() {
    }

    //Getters and setters

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getTeam_1_name() {
        return team_1_name;
    }

    public void setTeam_1_name(String team_1_name) {
        this.team_1_name = team_1_name;
    }

    public String getTeam_2_name() {
        return team_2_name;
    }

    public void setTeam_2_name(String team_2_name) {
        this.team_2_name = team_2_name;
    }
}
