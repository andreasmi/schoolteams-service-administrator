package com.noroff.service.administrator.models;

public class Postal {
    private int postalCode;
    private String postalCity;

    public Postal(int postalCode, String postalCity) {
        this.postalCode = postalCode;
        this.postalCity = postalCity;
    }

    public Postal() {
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCity() {
        return postalCity;
    }

    public void setPostalCity(String postalCity) {
        this.postalCity = postalCity;
    }
}
