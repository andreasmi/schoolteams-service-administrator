package com.noroff.service.administrator.models;

public class TeamDTO {
    private int id;

    public TeamDTO(int id) {
        this.id = id;
    }

    public TeamDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
