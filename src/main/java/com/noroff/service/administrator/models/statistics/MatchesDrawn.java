package com.noroff.service.administrator.models.statistics;

public class MatchesDrawn {
    private int id;
    private int matches_drawn;

    public MatchesDrawn(int id, int matches_drawn) {
        this.id = id;
        this.matches_drawn = matches_drawn;
    }

    public MatchesDrawn() {
    }

    public MatchesDrawn(int matches_drawn) {
        this.matches_drawn = matches_drawn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatches_drawn() {
        return matches_drawn;
    }

    public void setMatches_drawn(int matches_drawn) {
        this.matches_drawn = matches_drawn;
    }
}
