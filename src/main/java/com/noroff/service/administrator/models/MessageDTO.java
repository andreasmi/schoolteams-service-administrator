package com.noroff.service.administrator.models;

public class MessageDTO {
    private String message;
    private String sender;

    public MessageDTO(String message, String sender) {
        this.message = message;
        this.sender = sender;

    }

    public MessageDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
