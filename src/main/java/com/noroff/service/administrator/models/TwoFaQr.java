package com.noroff.service.administrator.models;

public class TwoFaQr {
    String url;

    public TwoFaQr(String url) {
        this.url = url;
    }

    public TwoFaQr() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
