package com.noroff.service.administrator.models;

import java.time.LocalDateTime;

public class FullUser {
    private String firstname;
    private String lastname;
    private String email;
    private String gender;
    private String username;
    private int role_id;
    private LocalDateTime date_of_birth;
    private String mobile_number;
    private String profile_picture;
    private String medical_note;

    //Constructors


    public FullUser(String firstname, String lastname, String email, String gender, String username, int role_id, LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_note) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gender = gender;
        this.username = username;
        this.role_id = role_id;
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_note = medical_note;
    }

    public FullUser(User user, Person person, Optional optional) {
        this.firstname = person.getFirstname();
        this.lastname = person.getLastname();
        this.email = person.getEmail();
        this.gender = person.getGender();
        this.username = user.getUsername();
        this.role_id = user.getRole_id();
        this.date_of_birth = optional.getDate_of_birth();
        this.mobile_number = optional.getMobile_number();
        this.profile_picture = optional.getProfile_picture();
        this.medical_note = optional.getMedical_notes();
    }

    //Getters and setters

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public LocalDateTime getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDateTime date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getMedical_note() {
        return medical_note;
    }

    public void setMedical_note(String medical_note) {
        this.medical_note = medical_note;
    }
}
