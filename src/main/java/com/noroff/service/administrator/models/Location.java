package com.noroff.service.administrator.models;

public class Location {
    private int id;
    private String street;
    private String number;
    private String name;
    private int postalCode_post_code;

    //Constructors
    public Location() {
    }

    public Location(int id, String street, String number, String name, int postalCode_post_code) {
        this.id = id;
        this.street = street;
        this.number = number;
        this.postalCode_post_code = postalCode_post_code;
        this.name = name;
    }

    public Location(String street, String number, String name, int postalCode_post_code) {
        this.street = street;
        this.number = number;
        this.postalCode_post_code = postalCode_post_code;
        this.name = name;
    }

    //Getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getPostalCode_post_code() {
        return postalCode_post_code;
    }

    public void setPostalCode_post_code(int postalCode_post_code) {
        this.postalCode_post_code = postalCode_post_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
