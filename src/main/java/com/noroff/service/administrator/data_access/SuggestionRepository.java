package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Suggestion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SuggestionRepository {

    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Suggestion> getAllSuggestions(){
        ArrayList<Suggestion> suggestions = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM school_change_suggestion");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                suggestions.add(new Suggestion(
                        result.getInt("school_id"),
                        result.getString("name"),
                        result.getString("comment"),
                        result.getBoolean("approved"),
                        result.getString("requester")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return suggestions;
    }

    public ArrayList<Suggestion> getAllPendingSuggestions(){
        ArrayList<Suggestion> suggestions = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM school_change_suggestion WHERE approved = false");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                suggestions.add(new Suggestion(
                    result.getInt("school_id"),
                    result.getString("name"),
                    result.getString("comment"),
                    result.getBoolean("approved"),
                    result.getString("requester")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return suggestions;
    }

    public ArrayList<Suggestion> getAllApprovedSuggestions(){
        ArrayList<Suggestion> suggestions = new ArrayList<>();

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM school_change_suggestion WHERE approved = true");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                suggestions.add(new Suggestion(
                        result.getInt("school_id"),
                        result.getString("name"),
                        result.getString("comment"),
                        result.getBoolean("approved"),
                        result.getString("requester")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return suggestions;
    }
}
