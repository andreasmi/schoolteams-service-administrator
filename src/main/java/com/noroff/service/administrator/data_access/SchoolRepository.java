package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.School;
import com.noroff.service.administrator.models.TeamDTO;
import com.noroff.service.administrator.models.User;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class SchoolRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all school
     * @return List of schools
     */
    public ArrayList<School> getAllSchools(){
        ArrayList<School> schools = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"school\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                schools.add(new School(
                        result.getInt("id"),
                        result.getString("name")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return schools;
    }


    /**
     * Get a specific school
     * @param id The school id
     * @return The school
     */
    public School getSpecificSchool(int id){
        School school = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"school\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                school = new School(
                        result.getInt("id"),
                        result.getString("name")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return school;
    }


    /**
     * Post a new school
     * @param school The school to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addSchool(School school){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.school(name)" +
                            " VALUES (?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, school.getName());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a location
     * @param id The school id
     * @param school The new school
     * @return Status of the request
     */
    public HttpStatus putSchool(int id, School school){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.school SET id=?, name=?" +
                            " WHERE id = ?;");
            prep.setInt(1, school.getId());
            prep.setString(2, school.getName());
            prep.setInt(3, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping

    //Gets all teams in a school

    /**
     * Gets all teams in a school
     * @param schoolId The school id
     * @return List of teams
     */
    public ArrayList<TeamDTO> getAllTeamsInSchool(int schoolId){
        ArrayList<TeamDTO> teams = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT id FROM public.\"team\" WHERE school_id = ?");
            prep.setInt(1, schoolId);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                teams.add(new TeamDTO(
                        result.getInt("id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return teams;
    }
}
