package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Location;
import com.noroff.service.administrator.models.Match;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.User;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class MatchRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all matches
     * @return List of matches
     */
    public ArrayList<Match> getAllMatches(){
        ArrayList<Match> matches = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"match\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                matches.add(new Match(
                        result.getInt("id"),
                        result.getObject("start_time", java.time.LocalDateTime.class),
                        result.getBoolean("cancelled"),
                        result.getString("comment"),
                        result.getInt("location_id"),
                        result.getInt("team_1_id"),
                        result.getInt("team_2_id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return matches;
    }

    /**
     * Get a specific match
     * @param id The match id
     * @return The match
     */
    public Match getSpecificMatch(int id){
        Match match = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"match\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                match = new Match(
                        result.getInt("id"),
                        result.getObject("start_time", java.time.LocalDateTime.class),
                        result.getBoolean("cancelled"),
                        result.getString("comment"),
                        result.getInt("location_id"),
                        result.getInt("team_1_id"),
                        result.getInt("team_2_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return match;
    }


    /**
     * Post a new match
     * @param match The match to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addMatch(Match match){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.match(start_time, cancelled, comment, location_id, team_1_id, team_2_id)" +
                            " VALUES (?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, match.getStart_time());
            prep.setBoolean(3, match.getCancelled());
            prep.setString(4, match.getComment());
            prep.setInt(5, match.getLocation_id());
            prep.setInt(6, match.getTeam_1_id());
            prep.setInt(7, match.getTeam_2_id());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     *
     * @param id The id of the match
     * @param match The new match
     * @return The status of the request
     */
    public HttpStatus putMatch(int id, Match match){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.match SET id=?, start_time=?, cancelled=?, comment=?, location_id=?, team_1_id=?, team_2_id=?" +
                            " WHERE id = ?;");
            prep.setInt(1, match.getId());
            prep.setObject(2, match.getStart_time());
            prep.setBoolean(3, match.getCancelled());
            prep.setString(4, match.getComment());
            prep.setInt(5, match.getLocation_id());
            prep.setInt(6, match.getTeam_1_id());
            prep.setInt(7, match.getTeam_2_id());
            prep.setInt(8, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping



    /**
     * Cancle a match
     * @param id The id of the match to cancel
     * @param message The message to send to the participents
     * @return The response code
     */
    public HttpStatus cancleMatch(int id, String message){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.match SET cancelled=?, comment=?" +
                            " WHERE id = ?;");
            prep.setBoolean(1, true);
            prep.setObject(2, message);
            prep.setInt(3, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

}
