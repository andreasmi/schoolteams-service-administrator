package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Message;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MessageRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    public ArrayList<Message> getAllMessages(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM message");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                    result.getInt("message_id"),
                    result.getString("receiver"),
                    result.getString("sender"),
                    result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    // Messages to/from administrator
    public ArrayList<Message> getAllMessagesFromAdministrator(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS sender " +
                    "ON message.sender = sender.username " +
                    "WHERE sender.role_id = 1) AS data");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesFromSpecificAdministrator(String username){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS sender " +
                    "ON message.sender = sender.username " +
                    "WHERE sender.role_id = 1 " +
                    "AND sender.username = ?) AS data");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesToAdministrator(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS receiver " +
                    "ON message.receiver = receiver.username " +
                    "WHERE receiver.role_id = 1) AS data");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesToSpecificAdministrator(String username){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS receiver " +
                    "ON message.receiver = receiver.username " +
                    "WHERE receiver.role_id = 1 " +
                    "AND receiver.username = ?) AS data");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesFromCoachToAdministrator(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS receiver " +
                    "ON message.receiver = receiver.username " +
                    "JOIN users AS sender " +
                    "ON message.sender = sender.username " +
                    "WHERE receiver.role_id = 1 AND sender.role_id = 2) AS data");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesFromParentToAdministrator(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS receiver " +
                    "ON message.receiver = receiver.username " +
                    "JOIN users AS sender " +
                    "ON message.sender = sender.username " +
                    "WHERE receiver.role_id = 1 AND sender.role_id = 3) AS data");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }

    public ArrayList<Message> getAllMessagesFromPlayerToAdministrator(){
        ArrayList<Message> messages = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM (SELECT sender, receiver, message, message_id FROM message " +
                    "JOIN users AS receiver " +
                    "ON message.receiver = receiver.username " +
                    "JOIN users AS sender " +
                    "ON message.sender = sender.username " +
                    "WHERE receiver.role_id = 1 AND sender.role_id = 4) AS data");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                messages.add(new Message(
                        result.getInt("message_id"),
                        result.getString("receiver"),
                        result.getString("sender"),
                        result.getString("message")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return messages;
    }
}
