package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Location;
import com.noroff.service.administrator.models.LocationFull;
import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Postal;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.*;

public class LocationRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;

    /**
     * Gets all locations
     * @return List of locations
     */
    public ArrayList<Location> getAllLocations(){
        ArrayList<Location> locations = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"location\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                locations.add(new Location(
                        result.getInt("id"),
                        result.getString("street"),
                        result.getString("number"),
                        result.getString("name"),
                        result.getInt("postal_code")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return locations;
    }


    /**
     * Get a specific location
     * @param id The location id
     * @return The location
     */
    public Location getSpecificLocation(int id){
        Location location = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"location\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                location = new Location(
                        result.getInt("id"),
                        result.getString("street"),
                        result.getString("number"),
                        result.getString("name"),
                        result.getInt("postal_code")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return location;
    }


    /**
     * Post a new location
     * @param location The location to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addLocation(Location location){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.location(street, \"number\", postal_code, name)" +
                            " VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, location.getStreet());
            prep.setString(2, location.getNumber());
            prep.setInt(3, location.getPostalCode_post_code());
            prep.setString(4, location.getName());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a location
     * @param id The location id
     * @param location The new location
     * @return Status of the request
     */
    public HttpStatus putLocation(int id, Location location){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.location SET id=?, street=?, \"number\"=?, postal_code=?, name=?" +
                            " WHERE id = ?;");
            prep.setInt(1, location.getId());
            prep.setString(2, location.getStreet());
            prep.setString(3, location.getNumber());
            prep.setInt(4, location.getPostalCode_post_code());
            prep.setString(5, location.getName());
            prep.setInt(6, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping



    /**
     * Gets all postals
     * @return List of postal
     */
    public ArrayList<Postal> getAllPostals(){
        ArrayList<Postal> postals = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"postal_code\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                postals.add(new Postal(
                        result.getInt("post_code"),
                        result.getString("post_city")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return postals;
    }


    /**
     * Get a specific postal
     * @param postalCode The postal code
     * @return The postal
     */
    public Postal getSpecificPostal(int postalCode){
        Postal postal = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"postal_code\" WHERE post_code = ?");
            prep.setInt(1, postalCode);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                postal = new Postal(
                        result.getInt("post_code"),
                        result.getString("post_city")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return postal;
    }



    /**
     * Get a full location
     * @return The full location
     */
    public ArrayList<LocationFull> getAllLocationFull(){
        ArrayList<LocationFull> locations = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT id, street, \"number\", postal_code, name, post_city" +
                    " FROM public.location" +
                    " INNER JOIN postal_code" +
                    " ON postal_code.post_code = location.postal_code;");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                locations.add(new LocationFull(
                        result.getInt("id"),
                        result.getString("street"),
                        result.getString("number"),
                        result.getString("name"),
                        result.getInt("postal_code"),
                        result.getString("post_city")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return locations;
    }

    /**
     * Get a full location, concatination of location and postal_code
     * @param id The location id
     * @return The location
     */
    public LocationFull getFullSpecificLocation(int id){
        LocationFull location = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT id, street, \"number\", postal_code, name, post_city" +
                    " FROM public.location" +
                    " INNER JOIN postal_code" +
                    " ON postal_code.post_code = location.postal_code" +
                    " WHERE location.id = ?;");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                location = new LocationFull(
                        result.getInt("id"),
                        result.getString("street"),
                        result.getString("number"),
                        result.getString("name"),
                        result.getInt("postal_code"),
                        result.getString("post_city")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return location;
    }

}
