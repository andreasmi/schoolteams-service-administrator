package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Person;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class PersonRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all persons
     * @return List of persons
     */
    public ArrayList<Person> getAllPersons(){
        ArrayList<Person> persons = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"person\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                persons.add(new Person(
                        result.getInt("id"),
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("email"),
                        result.getString("gender"),
                        result.getInt("optional_id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return persons;
    }


    /**
     * Get a specific person
     * @param id The person id
     * @return The person
     */
    public Person getSpecificPerson(int id){
        Person person = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"person\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                person = new Person(
                        result.getInt("id"),
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("email"),
                        result.getString("gender"),
                        result.getInt("optional_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return person;
    }


    /**
     * Post a new person
     * @param person The person to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addPerson(Person person){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.person(firstname, lastname, email, gender, optional_id)" +
                            " VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, person.getFirstname());
            prep.setString(2, person.getLastname());
            prep.setString(3, person.getEmail());
            prep.setString(4, person.getGender());
            prep.setInt(5, person.getOptional_id());


            int result = prep.executeUpdate();

            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a person
     * @param id The person id
     * @param person The new person
     * @return Status of the request
     */
    public HttpStatus putPerson(int id, Person person){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.person SET id=?, firstname=?, lastname=?, email=?, gender=?, optional_id=?" +
                            " WHERE id = ?;");
            prep.setInt(1, person.getId());
            prep.setString(2, person.getFirstname());
            prep.setString(3, person.getLastname());
            prep.setString(4, person.getEmail());
            prep.setString(5, person.getGender());
            prep.setInt(6, person.getOptional_id());
            prep.setInt(7, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping
}
