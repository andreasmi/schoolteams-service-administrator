package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Team;
import com.noroff.service.administrator.models.User;
import com.noroff.service.administrator.models.UserDTO;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class TeamRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all teams
     * @return List of teams
     */
    public ArrayList<Team> getAllTeams(){
        ArrayList<Team> teams = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"team\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                teams.add(new Team(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return teams;
    }


    /**
     * Get a specific team
     * @param id The team id
     * @return The team
     */
    public Team getSpecificTeam(int id){
        Team team = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"team\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                team = new Team(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return team;
    }


    /**
     * Post a new team
     * @param team The team to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addTeam(Team team){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.team(name, school_id, coach, sport_id)" +
                            " VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, team.getName());
            prep.setInt(2, team.getSchool_id());
            prep.setString(3, team.getCoach());
            prep.setInt(4, team.getSport_id());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a team
     * @param id The team id
     * @param team The new team
     * @return Status of the request
     */
    public HttpStatus putTeam(int id, Team team){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.team SET id=?, name=?, school_id=?, coach=?, sport_id=?" +
                            " WHERE id = ?;");

            prep.setInt(1, team.getId());
            prep.setString(2, team.getName());
            prep.setInt(3, team.getSchool_id());
            prep.setString(4, team.getCoach());
            prep.setInt(5, team.getSport_id());
            prep.setInt(6, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping

    /**
     * Get all players from a team
     * @param teamId The team id
     * @return List of user DTO
     */
    public ArrayList<UserDTO> getAllPlayers(int teamId) {
        ArrayList<UserDTO> players = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM users" +
                    " INNER JOIN player" +
                    " ON player.user_username = users.username" +
                    " WHERE player.team_id = ?;");
            prep.setInt(1, teamId);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                players.add(new UserDTO(
                        result.getString("username"),
                        result.getInt("person_id"),
                        result.getInt("role_id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return players;
    }

    public ArrayList<Team> getTeamBySchool(int school_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM team " +
                    "WHERE school_id = ?;");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamByCoach(String coach) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM team " +
                    "WHERE coach = ?;");
            prep.setString(1, coach);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }

    public ArrayList<Team> getTeamBySport(int sport_id) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM team " +
                    "WHERE sport_id = ?;");
            prep.setInt(1, sport_id);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                        new Team(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getInt("school_id"),
                                result.getString("coach"),
                                result.getInt("sport_id")
                        )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }
}
