package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Optional;
import com.noroff.service.administrator.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class OptionalRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all optionals
     * @return List of optional
     */
    public ArrayList<Optional> getAllOptionals(){
        ArrayList<Optional> optionals = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"optional\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                optionals.add(new Optional(
                        result.getInt("id"),
                        result.getObject("date_of_birth", java.time.LocalDateTime.class),
                        result.getString("mobile_number"),
                        result.getString("profile_picture"),
                        result.getString("medical_notes"),
                        result.getBoolean("dob_shared"),
                        result.getBoolean("mobile_number_shared"),
                        result.getBoolean("profile_picture_shared"),
                        result.getBoolean("medical_notes_shared")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optionals;
    }


    /**
     * Get a specific optional
     * @param id The optional id
     * @return The optional
     */
    public Optional getSpecificOptional(int id){
        Optional optional = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"optional\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                optional = new Optional(
                        result.getInt("id"),
                        result.getObject("date_of_birth", java.time.LocalDateTime.class),
                        result.getString("mobile_number"),
                        result.getString("profile_picture"),
                        result.getString("medical_notes"),
                        result.getBoolean("dob_shared"),
                        result.getBoolean("mobile_number_shared"),
                        result.getBoolean("profile_picture_shared"),
                        result.getBoolean("medical_notes_shared")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optional;
    }


    /**
     * Post a new optional
     * @param optional The optional to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addOptional(Optional optional){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.optional(date_of_birth, mobile_number, profile_picture, medical_notes)\n" +
                            " VALUES (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setObject(1, optional.getDate_of_birth());
            prep.setString(2, optional.getMobile_number());
            prep.setString(3, optional.getProfile_picture());
            prep.setString(4, optional.getMedical_notes());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    System.out.println(last_inserted_id);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a optional
     * @param id The optional id
     * @param optional The new optional
     * @return Status of the request
     */
    public HttpStatus putOptional(int id, Optional optional){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.optional SET id=?, date_of_birth=?, mobile_number=?, profile_picture=?, medical_notes=?, dob_shared=?, mobile_number_shared=?, profile_picture_shared=?, medical_notes_shared=?" +
                            " WHERE id = ?;");
            prep.setInt(1, optional.getId());
            prep.setObject(2, optional.getDate_of_birth());
            prep.setString(3, optional.getMobile_number());
            prep.setString(4, optional.getProfile_picture());
            prep.setString(5, optional.getMedical_notes());
            prep.setBoolean(6, optional.getDob_shared());
            prep.setBoolean(7, optional.getMobile_number_shared());
            prep.setBoolean(8, optional.getProfile_picture_shared());
            prep.setBoolean(9, optional.getMedical_notes_shared());
            prep.setInt(10, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping
}
