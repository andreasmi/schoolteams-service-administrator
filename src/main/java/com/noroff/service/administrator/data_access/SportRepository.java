package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.Pair;
import com.noroff.service.administrator.models.Sport;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class SportRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all sports
     * @return List of sports
     */
    public ArrayList<Sport> getAllSports(){
        ArrayList<Sport> sports = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"sport\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                sports.add(new Sport(
                        result.getInt("id"),
                        result.getString("name")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return sports;
    }


    /**
     * Get a specific sport
     * @param id The sport id
     * @return The sport
     */
    public Sport getSpecificSport(int id){
        Sport sport = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"sport\" WHERE id = ?");
            prep.setInt(1, id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                sport = new Sport(
                        result.getInt("id"),
                        result.getString("name")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return sport;
    }


    /**
     * Post a new sport
     * @param sport The sport to post
     * @return The http status of the request
     */
    public Pair<HttpStatus, Integer> addSport(Sport sport){

        HttpStatus success;
        int last_inserted_id;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.sport(name)" +
                            " VALUES (?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, sport.getName());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt(1);
                    return new Pair<>(success, last_inserted_id);
                }
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(success, -1);
    }

    /**
     * Put a sport
     * @param id The sport id
     * @param sport The new sport
     * @return Status of the request
     */
    public HttpStatus putSport(int id, Sport sport){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.sport SET id=?, name=?" +
                            " WHERE id = ?;");
            prep.setInt(1, sport.getId());
            prep.setString(2, sport.getName());
            prep.setInt(3, id);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping
}
