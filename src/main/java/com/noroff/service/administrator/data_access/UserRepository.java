package com.noroff.service.administrator.data_access;

import com.noroff.service.administrator.models.*;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UserRepository {
    //Setting up the connection object
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection conn = null;


    /**
     * Gets all users
     * @return List of users
     */
    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"users\"");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                users.add(new User(
                        result.getString("username"),
                        result.getString("password"),
                        result.getInt("person_id"),
                        result.getInt("role_id")
                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }


    /**
     * Get a specific user
     * @param username The users username
     * @return The user
     */
    public User getSpecificUser(String username){
        User user = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.\"users\" WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                user = new User(
                        result.getString("username"),
                        result.getString("password"),
                        result.getInt("person_id"),
                        result.getInt("role_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return user;
    }


    /**
     * Post a new user
     * @param user The user to post
     * @return The http status of the request
     */
    public HttpStatus addUser(User user){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.users(username, password, person_id, role_id)" +
                            " VALUES (?, ?, ?, ?);");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setInt(3, user.getPerson_id());
            prep.setInt(4, user.getRole_id());


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Put a user
     * @param username The users username
     * @param user The new user
     * @return Status of the request
     */
    public HttpStatus putUser(String username, User user){

        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET username=?, password=?, person_id=?, role_id=?" +
                            " WHERE username = ?;");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setInt(3, user.getPerson_id());
            prep.setInt(4, user.getRole_id());
            prep.setString(5, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    //TODO: Add delete mapping

    /**
     * Notify parents in a team
     * @param teamId The team id
     * @param message The message with a sender
     * @return Status of the request
     */
    public HttpStatus notifyParents(int teamId, MessageDTO message){
        HttpStatus status = HttpStatus.OK;
        ArrayList<String> parentUsername = new ArrayList<>();

        //Getting all usernames of the parents
        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT parent_1, parent_2" +
                    " FROM public.child_parent" +
                    " INNER JOIN users" +
                    " ON child_parent.child = users.username" +
                    " INNER JOIN player" +
                    " ON users.username = player.user_username" +
                    " WHERE player.team_id = ?;");
            prep.setInt(1, teamId);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                parentUsername.add(result.getString("parent_1"));
                if(result.getString("parent_2") != null && !result.getString("parent_2").equals("null")){
                    parentUsername.add(result.getString("parent_2"));
                }
            }

        } catch (Exception e){
            System.out.println(e.toString());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }
        System.out.println(parentUsername.size());
        for(int i = 0; i < parentUsername.size(); i++) {
            System.out.println(parentUsername.get(i));
            if(parentUsername.get(i) != null || !parentUsername.get(i).equals("null")){
                sendMessage(message.getSender(), parentUsername.get(i), message.getMessage());
            }
        }

        return status;
    }


    /**
     * Send message from one sender to one reciever
     * @param senderUsername The senders username
     * @param recieverUsername The recievers username
     * @param message The message
     * @return The status of the request
     */
    public HttpStatus sendMessage(String senderUsername, String recieverUsername, String message){
        HttpStatus success;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO public.message(sender, receiver, message)" +
                            " VALUES (?, ?, ?);");
            prep.setString(1, senderUsername);
            prep.setString(2, recieverUsername);
            prep.setString(3, message);


            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Gets all users
     * @return List of users
     */
    public ArrayList<FullUser> getAllCoaches(){
        ArrayList<FullUser> users = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT username, role_id, firstname, lastname, email, gender, date_of_birth, mobile_number, profile_picture, medical_notes" +
                    " FROM public.users" +
                    " INNER JOIN person" +
                    " ON person.id = users.person_id" +
                    " INNER JOIN optional" +
                    " ON optional.id = person.optional_id" +
                    " WHERE role_id = 2;");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                users.add(new FullUser(
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("email"),
                        result.getString("gender"),
                        result.getString("username"),
                        result.getInt("role_id"),
                        result.getObject("date_of_birth", java.time.LocalDateTime.class),
                        result.getString("mobile_number"),
                        result.getString("profile_picture"),
                        result.getString("medical_notes")

                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    /**
     * Gets all users
     * @return List of users
     */
    public ArrayList<FullUser> getAllAdministrators(){
        ArrayList<FullUser> users = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT username, role_id, firstname, lastname, email, gender, date_of_birth, mobile_number, profile_picture, medical_notes" +
                    " FROM public.users" +
                    " INNER JOIN person" +
                    " ON person.id = users.person_id" +
                    " INNER JOIN optional" +
                    " ON optional.id = person.optional_id" +
                    " WHERE role_id = 1;");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                users.add(new FullUser(
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("email"),
                        result.getString("gender"),
                        result.getString("username"),
                        result.getInt("role_id"),
                        result.getObject("date_of_birth", java.time.LocalDateTime.class),
                        result.getString("mobile_number"),
                        result.getString("profile_picture"),
                        result.getString("medical_notes")

                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    /**
     * Gets all users
     * @return List of users
     */
    public ArrayList<FullUser> getAllParents(){
        ArrayList<FullUser> users = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT username, role_id, firstname, lastname, email, gender, date_of_birth, mobile_number, profile_picture, medical_notes" +
                    " FROM public.users" +
                    " INNER JOIN person" +
                    " ON person.id = users.person_id" +
                    " INNER JOIN optional" +
                    " ON optional.id = person.optional_id" +
                    " WHERE role_id = 3;");
            ResultSet result = prep.executeQuery();

            while(result.next()){
                users.add(new FullUser(
                        result.getString("firstname"),
                        result.getString("lastname"),
                        result.getString("email"),
                        result.getString("gender"),
                        result.getString("username"),
                        result.getInt("role_id"),
                        result.getObject("date_of_birth", java.time.LocalDateTime.class),
                        result.getString("mobile_number"),
                        result.getString("profile_picture"),
                        result.getString("medical_notes")

                ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    /**
     * Post a new child parent relationship
     * @param childParent The child parent relationship
     * @return The response status
     */
    public HttpStatus addChildParent(ChildParent childParent){

        HttpStatus success;

        try {
            boolean childExists = false;
            conn = DriverManager.getConnection(URL);
            PreparedStatement prepSelect =
                    conn.prepareStatement("SELECT * FROM public.child_parent WHERE child = ?");
            prepSelect.setString(1, childParent.getChildUsername());

            ResultSet resultSet = prepSelect.executeQuery();

            while(resultSet.next()){
                childExists = true;
            }

            PreparedStatement prep;
            if(childExists){
                prep =
                        conn.prepareStatement("UPDATE public.child_parent SET parent_1=?, parent_2=? WHERE child = ?;");
                prep.setString(1, childParent.getParent1Username());
                prep.setString(2, childParent.getParent2Username());
                prep.setString(3, childParent.getChildUsername());
            }else{
                prep =
                        conn.prepareStatement("INSERT INTO public.child_parent(parent_1, child, parent_2)" +
                                " VALUES (?, ?, ?);");
                prep.setString(1, childParent.getParent1Username());
                prep.setString(2, childParent.getChildUsername());
                prep.setString(3, childParent.getParent2Username());
            }

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.CREATED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Get child parent relationship
     * @param childUsername The childs username
     * @return The child parent relationship
     */
    public ChildParent getChildParent(String childUsername){
        ChildParent childParent = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT * FROM public.child_parent WHERE child = ?");
            prep.setString(1, childUsername);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                childParent = new ChildParent(
                    result.getString("child"),
                        result.getString("parent_1"),
                        result.getString("parent_2")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return childParent;
    }

    /**
     * Toggle the two-factor-authentication active flag for a user
     * @param toggle2FA The username and true/false
     * @return The status of the flag
     */
    public Boolean update2FA(Toggle2FA toggle2FA){
        Boolean success = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE public.users SET \"2fa_active\"=?" +
                            " WHERE username = ?;");
            prep.setBoolean(1, toggle2FA.getActive());
            prep.setString(2, toggle2FA.getUsername());

            int result = prep.executeUpdate();
            if(result != 0){
                success = toggle2FA.getActive();
            }else{
                success = null;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    /**
     * Gets the qr code for a user
     * @param username The users username
     * @return The qr code url
     */
    public String getQrUrl(String username){
        String url = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT tfa_qr_url FROM public.\"users\" WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                url = result.getString("tfa_qr_url");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return url;
    }

    /**
     * Gets the qr code for a user
     * @param username The users username
     * @return The qr code url
     */
    public Boolean getActive(String username){
        Boolean active = null;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement("SELECT \"2fa_active\" FROM public.\"users\" WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                active = result.getBoolean("2fa_active");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                conn.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return active;
    }

}
