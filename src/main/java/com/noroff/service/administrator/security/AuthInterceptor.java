package com.noroff.service.administrator.security;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter implements WebMvcConfigurer {

    /**
     * preHandler for service. Makes sure that the request isn't coming from anyone unauthorized
     * @param request The request to forward to an endpoint
     * @param response The response to send back
     * @param handler The handler to use if the preHandler returns true
     * @return Boolean if the request is authorized or not
     * @throws Exception The abstract extension might throw an exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                              HttpServletResponse response, Object handler) throws Exception{

        //Gets the token and checks if it exists
        String tokenString = request.getHeader("Authorization");
        if(tokenString == null || tokenString.isEmpty()){
            response.setStatus(401);
            return false;
        }

        //Sends request to verify the token
        String url = "https://schoolteams-service-auth.herokuapp.com/api/v1/verify";
        RestTemplate restTemplate = new RestTemplate();

        Map<String, String> requestBody = new HashMap();
        requestBody.put("tokenstring", tokenString);
        requestBody.put("service", "administrator");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<?> responseEntity = restTemplate.postForEntity(
                url,
                requestEntity,
                String.class);
        HttpStatus statusCode = responseEntity.getStatusCode();
        //If the response code is 200, then the token is valid
        boolean authorized = statusCode == HttpStatus.OK;

        if(authorized){
            return super.preHandle(request, response, handler);
        }else {
            response.setStatus(401);
            return false;
        }


    }

    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor());
    }*/

}
