# School Teams

## Team Members
* Lars Henrik Skage (https://gitlab.com/lhskage)
* Tosif Bhatti (https://gitlab.com/tbhatt)
* Charlotte Ødegården (https://gitlab.com/codega)
* Jostein Olstad (https://gitlab.com/josteiol)
* Andreas Mikalsen (https://gitlab.com/andreasmi)

## Installation instructions
Every micro-service for the original application were deployed to heroku. This can cause the application to be slow if there are no activity for a while. One can deploy them on other services but this has not been tested or validated by the team members.

### Steps:
1. Generate database with the given .sql file.
2. Deploya every micro-service seperatly.
3. Configure the url variables and system varaibles for every service.
    * Eureka url varaible
    * Connection string for database
    * Gmail password
    * JWT Encryption Key
    * JWT Issuer
    * JWT Two Factor Authentication Issuer
4. Start inserting data to the database in the given order.

## API Documentation
Every api request from the frontend application should go through the gateway except for the mail- and auth-service. This is because the authentication of the application is token based. The mail- and auth-services does not have checks for the token because this should be accessible without needing to login. The documentation for the gateway only shows how to forward the request to the correct service. The other api documentation is pointing directly to the service it self, but when it is used it should go throught the gateway.
* Auth service: https://documenter.getpostman.com/view/5518775/TVYF9yu9
* Mail service: https://documenter.getpostman.com/view/6378231/TVRrU3xn
* Api Gateway: https://documenter.getpostman.com/view/6378231/TVYDfzcq
* Administrator service: https://documenter.getpostman.com/view/6378231/TVYDdJgw
* Coach service: https://documenter.getpostman.com/view/10398609/TVRq36Px
* Parent service: https://documenter.getpostman.com/view/10398609/TVRoY6fi
* Player service: https://documenter.getpostman.com/view/10398609/TVRkY7NN
* GDPR service: https://documenter.getpostman.com/view/10398609/TVYDcyMu
